const { test } = require('blue-tape')
const { join } = require('path')
const { readFileSync } = require('fs')
const unbundle = require('../unbundle.js')
const { tmpdir } = require('os')
const sortBy = require('lodash.sortby')

const scenarios = [
  {
    label: 'Contrived example using a dummy app',
    given: 'fixtures/dummy-app/given',
    expected: 'fixtures/dummy-app/expected',
    entry: 'src/entry.js',
    fixtures: [
      { source: 'node_modules/sister-module/sister-module.js',
        to: 'node_modules/sister-module/sister-module.js' },
      { source: 'node_modules/npm-module/npm-module.js',
        to: 'node_modules/npm-module/npm-module.js' },
      { source: 'src/child-module/bar.js',
        to: 'child-module/bar.js' },
      { source: 'src/child-module/foo.js',
        to: 'child-module/foo.js' },
      { source: 'src/entry.js',
        to: 'entry.js',
        map: 'entry.js.map' }
    ]
  },
  {
    label: 'Dynamic imports with string literals',
    given: 'fixtures/dynamic-import/given',
    expected: 'fixtures/dynamic-import/expected',
    entry: 'entry.js',
    fixtures: [
      { source: 'entry.js',
        to: 'entry.js' },
      { source: 'foo.js',
        to: 'foo.js' }
    ]
  }
]

for (const scenario of scenarios) {
  test(scenario.label, async (t) => {
    const entry = join(__dirname, scenario.given, scenario.entry)
    const destination = tmpdir()
    const actual = unbundle(entry, destination, { verbose: true })
    const expected = scenario.fixtures.map(({ source, to, map }) => {
      const codePath = join(__dirname, scenario.expected, to)
      return {
        source: join(__dirname, scenario.given, source),
        to: join(destination, to),
        map: map || false,
        code: readFileSync(codePath).toString()
      }
    })
    t.deepEqual(
      sortBy(Array.from(actual.values()), 'source'),
      sortBy(expected, 'source')
    )
  })
}
