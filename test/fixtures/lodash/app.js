/* eslint-env browser */
import _ from 'lodash-es'

_.forEach([1, 2, 3], (number) => {
  document.body.append(
    document.createTextNode(number)
  )
})
