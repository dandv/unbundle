/* eslint-env browser */
import moment from 'moment'

const root = document.getElementById('root')
root.innerText = `Time now: ${moment().format()}!`
