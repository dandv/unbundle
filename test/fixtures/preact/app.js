/* eslint-env browser */
import { h, render } from 'preact'

render(
  h('div', { class: 'hi' }, 'Hello World rendered with preact!'),
  document.body
)
