/* eslint-env browser */
import { LitElement, html }
  from 'lit-element'

class MyElement extends LitElement {
  constructor () {
    super()
    this.mood = 'great'
  }

  render () {
    return html`Web Components are <span>${this.mood}</span>!`
  }
}

customElements.define('my-element', MyElement)

document.body.appendChild(document.createElement('my-element'))
