const { join, extname, resolve, dirname } = require('path')
const { createServer } = require('http')
const { createReadStream } = require('fs')

function getMime (extension) {
  switch (extension) {
    case '.html': return 'text/html'
    case '.css': return 'text/css'
    case '.js':
    case '.mjs': return 'application/javascript'
    case '.map':
    case '.json': return 'application/json'
  }
}

function serve (filepath, response) {
  const file = createReadStream(filepath)
  file.pipe(response)
  file.on('error', (error) => {
    switch (error.code) {
      case 'ENOENT':
        response.writeHead(404)
        response.end('Not Found')
        break
      default:
        response.writeHead(500)
        response.end('Internal Server Error')
    }
  })
}

module.exports =
async function serveStatic (processed, destination, cwd) {
  const server = createServer({}, (request, response) => {
    console.log(request.method, request.url)
    const pathname = request.url.endsWith('/')
      ? `${request.url}index.html` : request.url
    const contentType = getMime(extname(pathname))
    if (contentType) {
      response.setHeader('content-type', contentType)
    }
    for (const resource of processed.values()) {
      if (resource.to === join(destination, pathname)) {
        response.end(resource.code)
        return
      } else if (resource.map) {
        const sourcemap = resolve(dirname(resource.source), resource.map)
        if (sourcemap.substr(destination.length) === pathname) {
          serve(sourcemap, response)
          return
        }
      }
    }
    serve(join(cwd, pathname), response)
  })
  return server
}
