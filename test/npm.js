const { join } = require('path')
const unbundle = require('..')
const os = require('os')
require('events.once/polyfill')
const { once } = require('events')
const puppeteer = require('puppeteer')
const assert = require('assert')
const serveStatic = require('./helpers/serveStatic')
const { readdir, stat } = require('fs').promises
const { execSync } = require('child_process')

process.on('unhandledRejection', (error) => {
  console.error(error)
  process.exit(1)
})

;(async () => {
  const fixtures = join(__dirname, 'fixtures')
  for (const fixture of await readdir(fixtures)) {
    // Skip failing test case pending upstream fix
    // https://github.com/graphql/graphql-js/issues/1819
    if (fixture === 'graphql') continue

    // Uncomment to test specific case
    // if (fixture !== '...') continue

    const cwd = join(fixtures, fixture)
    try {
      await stat(join(cwd, 'package.json'))
    } catch (error) { continue }
    console.log('Testing:', fixture)
    execSync('npm i', { cwd, stdio: 'inherit' })
    const { main } = require(join(cwd, 'package.json'))
    const entry = join(cwd, main)
    const destination = os.tmpdir()
    const actual = unbundle(entry, destination, { verbose: true })
    const server = await serveStatic(actual, destination, cwd)
    server.listen(0)
    await once(server, 'listening')
    console.log(`Serving at http://localhost:${server.address().port}`)

    // Uncomment to wait and debug manually in a browser
    // return

    const browser = await puppeteer.launch({
      headless: true,
      args: ['--headless', '--no-sandbox']
    })
    const page = await browser.newPage()
    await page.goto(
      `http://localhost:${server.address().port}`,
      { waitUntil: ['load', 'networkidle0'] }
    )
    switch (fixture) {
      case 'd3':
        assert.strictEqual(
          await page.$$eval('svg .bar', (bars) => bars.length),
          14
        )
        break
      case 'graphql':
        assert.strictEqual(
          await page.$eval('body', (body) => body.textContent.trim()),
          JSON.stringify({ data: { hello: 'world' } })
        )
        break
      case 'lit-element':
        assert.strictEqual(
          await page.$eval(
            'my-element',
            (element) => element.shadowRoot.textContent
          ),
          'Web Components are great!'
        )
        break
      case 'lodash':
        assert.strictEqual(
          await page.$eval('body', (body) => body.textContent.trim()),
          '123'
        )
        break
      case 'moment':
        assert.ok(
          await page.$eval(
            '#root',
            (div) => div.textContent.includes('Time now: ')
          )
        )
        break
      case 'popper.js':
        assert.strictEqual(
          await page.$eval('#popper', (popper) => popper.style.position),
          'absolute'
        )
        break
      case 'preact':
        assert.strictEqual(
          await page.$eval('.hi', (div) => div.textContent),
          'Hello World rendered with preact!'
        )
        break
      case 'ramda':
        assert.strictEqual(
          await page.$eval('body', (body) => body.textContent.trim()),
          '123'
        )
        break
      case 'vue':
        assert.strictEqual(
          await page.$eval('#app', (app) => app.textContent.trim()),
          'Hello Vue!'
        )
        break
      case 'whatwg-fetch':
        assert.deepStrictEqual(
          JSON.parse(
            await page.$eval('body', (body) => body.textContent.trim())
          ),
          require(join(cwd, 'package.json'))
        )
        break
      default:
        throw new Error('Missing validation check')
    }
    console.log('ok test passed')
    await page.close()
    await browser.close()
    server.close()
    await once(server, 'close')
  }
})()
